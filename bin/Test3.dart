import 'dart:convert';
import 'dart:io';
import "dart:math";

import '../lib/lib.dart';
import 'SparseGridImplementation.dart';

main(List<String> args) {
  if (args.length == 0) {
    print("no file name given");
  }
  List list = json.decode(new File(args[0]).readAsStringSync());
  List<IndexEntry<int>> entries = new List();
  IndexEntry<int> index;
  for (var l in list) {
    if (l["type"] == "point") {
      index = new IndexEntry(
          int.parse(l["name"]), [new Point(l["x"].round(), l["y"].round())]);
    } else {
      index = new IndexEntry(
          int.parse(l["name"]), [new PolyLine.fromJSON(l["points"])]);
    }
    entries.add(index);
  }
  /*RTreeSpatialIndex<int> spatialIndex = new RTreeSpatialIndex();
  spatialIndex.rtree.constructed = true;
  entries.shuffle();*/
  SparseGridSpatialIndex<int> spatialIndex = new SparseGridSpatialIndex();
  spatialIndex.index(entries);
  //spatialIndex.rtree.constructed = true;
  spatialIndex.display();


  /*for (List<Geom> query in queries) {
    spatialIndex()
  }*/


}