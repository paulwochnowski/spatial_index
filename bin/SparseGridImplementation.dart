import '../SparseGrid/SparseGrid.dart';
import '../lib/lib.dart';


class SparseGridSpatialIndex<T> implements SpatialIndex<T> {

  SparseGrid<T> grid;

  SparseGridSpatialIndex() {
    //200,200 for elering511
    //50,50 for eastlakes
    this.grid = new SparseGrid(10,10);
  }
  @override
  void index(List<IndexEntry<T>> entries) {
    for (IndexEntry<T> entry in entries) {
      grid.insert(entry);
    }
  }


  @override
  List<T> getIntersections(List<Geom> region) {
    return List.from(grid.query(region));
  }

  @override
  void display() {
    grid.display();
  }
  int depth() {
    return -1;
  }

  @override
  void remove(List<T> objects) {
    for (T object in objects) {
      grid.remove(object);
    }
  }

}
