import "../lib/lib.dart";
import "../RTree/RTree.dart";
import 'dart:io';

class RTreeSpatialIndex<T> implements SpatialIndex<T> {
  RTree<T> rtree;

  RTreeSpatialIndex.bulkInsert() {
    rtree = new RTree();
    rtree.constructed = false;
  }

  RTreeSpatialIndex() {
    rtree = new RTree();
  }

  void index(List<IndexEntry<T>> entries) {
    if (!rtree.constructed) {
      List<Leaf<T>> expanded = new List();

      for (IndexEntry<T> entry in entries) {
        for (Geom g in entry.regions) {
          expanded.add(new Leaf(g.getMinBoundingRectangle(), entry.object));
        }
      }
      rtree.bulkInsert(expanded);
    } else {
      for (IndexEntry<T> entry in entries) {
        for (Geom g in entry.regions) {
          rtree.insert(new Leaf(g.getMinBoundingRectangle(), entry.object));
        }
      }
    }
  }

  void display() {
    File logFile = new File('log');
    rtree.draw(logFile);
  }

  int depth() {
    return rtree.depth();
  }

  void remove(List<T> objects) {
    for (T object in objects) {
      rtree.remove(object);
    }
  }

  List<T> getIntersections(List<Geom> regions) {
    List<T> ret = new List();
    for (Geom region in regions) {
      ret.addAll(rtree.getIntersections(region.getMinBoundingRectangle()));
    }
    return ret;
  }
}
