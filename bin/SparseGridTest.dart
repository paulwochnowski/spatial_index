import 'SparseGridImplementation.dart';
import '../lib/lib.dart';

main() {
  List<Vector> linePoints1 = new List();
  List<Vector> linePoints2 = new List();
  List<Vector> linePoints3 = new List();
  for (int i = 0; i < 100; i++) {
    linePoints1.add(new Vector(1, i*6));
    linePoints2.add(new Vector(550, i*5));
    if (i > 5) continue;
    linePoints3.add(new Vector(20+i*7, 30+i*11));
  }
  List<IndexEntry<int>> insert = new List();
//  insert.add(new IndexEntry(4, [new PolyLine(linePoints3)]));
//  insert.add(new IndexEntry(0, [new PolyLine(linePoints1)]));
//  insert.add(new IndexEntry(1, [new PolyLine(linePoints2)]));
  insert.add(new IndexEntry(5, [new PolyLine([new Vector(10, 11), new Vector(10, 16)])]));
  insert.add(new IndexEntry(6, [new PolyLine([new Vector(0, 0), new Vector(10, 0)])]));
  insert.add(new IndexEntry(2, [new PolyLine([new Vector(0, -20), new Vector(11, -20)])]));
  insert.add(new IndexEntry(0, [new Point(2,0)]));
//  insert.add(new IndexEntry(6, [new Rect(new Vector(50, 10), new Vector(50, 50))]));
//  insert.add(new IndexEntry(2, [new Point(100, 200), new Point(600, 600)]));
  SparseGridSpatialIndex<int> index = new SparseGridSpatialIndex();
  index.index(insert);
  index.display();
  Rect r = new Rect(new Vector(2,0), new Vector(2,4));
  index.getIntersections([r]).forEach(print);
}