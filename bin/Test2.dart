import "../lib/lib.dart";
import "RTreeSpatialIndex.dart";

main() {
  SpatialIndex<String> test = new RTreeSpatialIndex();

  Geom shape = new Rect(new Vector(1, 1), new Vector(1, 1));
  List<Geom> reg = new List();
  reg.add(shape);
  IndexEntry<String> index = new IndexEntry("box", reg);
  List<IndexEntry<String>> entries = [index];

  Geom shape2 = new Rect(new Vector(3, 3), new Vector(1, 1));
  List<Geom> reg2 = new List();
  reg2.add(shape2);
  IndexEntry<String> index2 = new IndexEntry("box2", reg2);
  List<IndexEntry<String>> entries2 = [index2];

  test.index(entries);
  test.index(entries2);
  print(reg.length);

  printer(test, reg);
  printer(test, reg2);

  Geom shape3 = new Rect(new Vector(1, 1), new Vector(3, 3));
  List<Geom> reg3 = new List();
  reg3.add(shape3);
  IndexEntry<String> index3 = new IndexEntry("box3", reg3);
  List<IndexEntry<String>> entries3 = [index3];

  test.index(entries3);
  printer(test, reg);
  printer(test, reg2);
  printer(test, reg3);
  Geom shape4 = new Rect(new Vector(1, 1), new Vector(4, 4));
  IndexEntry<String> index4 = new IndexEntry("box4", [shape4]);
  List<IndexEntry<String>> entries4 = [index4];
  test.index(entries4);
  test.display();
  test.remove(entries4.map((entry) => entry.object));
  print("\n");
  test.display();
}

printer(SpatialIndex test, List<Geom> reg) {
  List<String> res = test.getIntersections(reg);
  print(res.length);
  res.forEach(print);
}
