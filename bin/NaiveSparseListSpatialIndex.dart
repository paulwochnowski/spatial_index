import "../lib/lib.dart";

class NaiveSparseListSpatialIndex<T> implements SpatialIndex<T> {
  List<IndexEntry<T>> store;
  int offsetX;
  int offsetY;

  NaiveSparseListSpatialIndex(int oX, int oY) {
    this.store = new List();
    offsetX = oX;
    offsetY = oY;
  }

  void index(List<IndexEntry<T>> entries) {
    for (IndexEntry<T> entry in entries) {
      store.add(entry);
    }
  }

  void remove(List<T> objects) {
    for (T object in objects) {
      for (IndexEntry<T> entry in this.store) {
        if (entry.object == object) {
          store.remove(entry);
          break;
        }
      }
    }
  }

  List<T> getIntersections(List<Geom> regions) {
    List<T> res = new List();
    for (Geom region in regions) {
      MinBoundingRectangle mbr = region.getMinBoundingRectangle();
      print(mbr);
      Vector botLeft = new Vector((mbr.minX/this.offsetX).floor()*this.offsetX,
                                    (mbr.minY/this.offsetY).floor()*this.offsetY);
      Vector topRight = new Vector((mbr.maxX/this.offsetX).ceil()*this.offsetX,
                                    (mbr.maxY/this.offsetY).ceil()*this.offsetY);
      mbr = new MinBoundingRectangle(botLeft, topRight - botLeft);
      for (IndexEntry<T> s in this.store) {
        if (mbr.intersects(s.getMinBoundingRectangle())) res.add(s.object);
      }
    }
    return res;
  }

  void display() {
    print("nothing to see here :-)");
  }
  int depth() {
    return -1;
  }
}