library Tester;

import "dart:io";
import "dart:math";
import "dart:collection";

import "../lib/lib.dart";
import "ListSpatialIndex.dart";
import "RTreeSpatialIndex.dart";
import "SparseGridImplementation.dart";
import "NaiveSparseListSpatialIndex.dart";

Random rand;
int id = 0;
bool clustered;
bool fromFile = false;
String file;
bool addPoints;
List<Vector> pointsToAdd;

List<List<Geom>> makeQueries(int count, int x, int y) {
  List<List<Geom>> queries = new List();
  for (int i = 0; i < count; i++) {
    queries.add(makeRegion(x, y));
  }
  return queries;
}

List<IndexEntry<Identifier>> makeEntries(int count, int x, int y) {
  List<IndexEntry<Identifier>> entries = new List();
  for (int i = 0; i < count; i++) {
    entries.add(makeEntry(x, y));

  }
  return entries;
}
int getRandSize() {
  int size = rand.nextInt(20);
  if (size == 19) {
    size = 100;
  } else if (size <= 8 || size >= 5) {
    size = 30;
  } else if (size <= 4 || size >= 0) {
    size = 3;
  }
  return size;
}

int getRandCoord(int centre) {
  if (centre == 0) return 0;
  int gap = rand.nextInt(centre);

  return rand.nextInt(centre) - 2*gap;
}


IndexEntry<Identifier> makeEntry(int x, int y) {
  int chance = rand.nextInt(100);
  if (chance < 2) {
    return new IndexEntry(new Identifier(id++), makeRegion(x, y));
  }
  //draw a line
  int samplePoints = rand.nextInt(10) + 2;
  List<Vector> points = new List();
  for (int i = 0; i < samplePoints; i++) {
    double theta = pi*(rand.nextDouble()*2 - 1);
    points.add(new Vector(x + (i*cos(theta)).round(), y + (i*sin(theta)).round()));
  }
  if (chance < 4) {
    clustered = true;
    pointsToAdd = new List.from(points);
  }
  return new IndexEntry(new Identifier(id++), [new PolyLine(points)]);
}

List<Geom> makeRegion(int x, int y) {
  return [new Rect(new Vector(getRandCoord(x.abs())*x.sign, getRandCoord(y.abs())*y.sign),
                   new Vector(getRandSize(), getRandSize()))];
}

//Vector makeRandVector(int mul) {
//  return new Vector(rand.nextInt(4*mul), rand.nextInt(4*mul));
//}

main(List<String> args) {
  String indexTypes = "nS/nR/r/b/g",
      endAfter = "i/q",
      display = "y/n";
  if (args.length < 5) {
    print(
        "not enough args: expected (index type: ${indexTypes}; #elements: num; seed: num; end after: ${endAfter}; display: ${display})");
    exit(1);
  }
  SpatialIndex<Identifier> index;
  bool isBulk = false;
  switch (args[0]) {
    case "nR": // naive list
      index = new ListSpatialIndex();
      break;
    case "nS":
      index = new NaiveSparseListSpatialIndex(10, 10);
      break;
    case "r": // rtree individual insertion
      index = new RTreeSpatialIndex();
      break;
    case "b": // rtree bulk insertion
      index = new RTreeSpatialIndex.bulkInsert();
      isBulk = true;
      break;
    case "g": // sparse grid
      index = new SparseGridSpatialIndex();
      break;
    default:
      print("Index type not recognised: expected ${indexTypes}");
      exit(1);
  }
  int elemCount = int.parse(args[1]);

  int seed = int.parse(args[2]);

  if (seed <= 0) {
    rand = new Random();
    seed = rand.nextInt(100000);
  }
  print("Seed: $seed");
  rand = new Random(seed);

  String endAt = args[3];
  if (!(endAt == "i" || endAt == "q")) {
    print("End time not recognised: expected ${endAfter}");
    exit(1);
  }
  String displayTree = args[4];
  if (!(displayTree == "y" || displayTree == "n")) {
    print("Display tree value not recognised: expected ${display}");
    exit(1);
  }


  List<IndexEntry<Identifier>> entries = new List();
  List<List<Geom>> queries = new List();
  Map<int, List<Geom>> m = new HashMap();

  addPoints = false;
  Vector currPoint = new Vector(1,1);
  double theta = pi*(rand.nextDouble()*2 - 1);
  Vector dir = new Vector((20*cos(theta)).round(), (20*sin(theta)).round());
  for (int x = 0; x < elemCount; x++) {
    entries.add(makeEntry(currPoint.x, currPoint.y));
    m[entries[x].object.id] = new List.from(entries[x].regions);
    queries.add(makeRegion(currPoint.x, currPoint.y));
    if (addPoints) {
      addPoints = false;
      for (Vector p in pointsToAdd) {
        entries.add(new IndexEntry(new Identifier(id++), [new Point(p.x, p.y)]));
        if (++x >= elemCount) {
          break;
        }
      }
    }
    if (x % 100 == 0) {
      theta = pi*(rand.nextDouble()*2 - 1);
      dir = new Vector((20*cos(theta)).round(), (20*sin(theta)).round());
    }
    if (x%300 == 0) {
      currPoint = new Vector.zero();
    }
    currPoint += dir;
  }
//  entries.add(new IndexEntry(new Identifier(id++), [new Point(0,0)]));
//  entries.add(new IndexEntry(new Identifier(id++), [new Point(0,0)]));
//  entries.add(new IndexEntry(new Identifier(id++), [new Point(0,0)]));
//  entries.add(new IndexEntry(new Identifier(id++), [new Point(0,0)]));
//  index.index(entries);

  entries.shuffle(rand);
  Map<Identifier, List<Geom>> map = new HashMap();
  if (isBulk) {
    print(entries.length);
    index.index(entries);
  } else {
    for (int i = 0; i < elemCount;) {
      int r = rand.nextInt(3);
      if (r == 0) {
        if (i ==0) continue;
        int delete = rand.nextInt(i);
        index.remove([entries[delete].object]);
      } else {
        index.index([entries[i++]]);
      }
    }
  }



  if (displayTree == "y") {
    index.display();
  }

  if (endAt == "i") return;

  int intersections = 0;
  for (List<Geom> query in queries) {
    print("\nNew Entry: ${query}\n");
    List<Identifier> res = index.getIntersections(query);
    intersections += res.length;
    res.sort(identCompare);
    res.forEach((res) => print("${res.id}: ${m[res.id][0]}"));
  }
//  print(intersections);

  if (endAt == "q") return;
}

int identCompare(Identifier a, Identifier b) {
  return a.id.compareTo(b.id);
}

class Identifier {
  int id;
  Identifier(this.id);
}