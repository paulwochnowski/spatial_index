import "../lib/lib.dart";

class ListSpatialIndex<T> implements SpatialIndex<T> {
  List<IndexEntry<T>> store;

  ListSpatialIndex() {
    this.store = new List();
  }

  void index(List<IndexEntry<T>> entries) {
    for (IndexEntry<T> entry in entries) {
      store.add(entry);
    }
  }

  void remove(List<T> objects) {
    for (T object in objects) {
      for (IndexEntry<T> entry in this.store) {
        if (entry.object == object) {
          store.remove(entry);
          break;
        }
      }
    }
  }

  List<T> getIntersections(List<Geom> regions) {
    List<T> res = new List();
    for (Geom region in regions) {
      MinBoundingRectangle mbr = region.getMinBoundingRectangle();
      for (IndexEntry<T> s in this.store) {
        if (mbr.intersects(s.getMinBoundingRectangle())) res.add(s.object);
      }
    }
    return res;
  }

  void display() {
    print("nothing to see here :-)");
  }
  int depth() {
    return -1;
  }
}