import "../lib/lib.dart";
import "ListSpatialIndex.dart";

main() {
  SpatialIndex<String> test = new ListSpatialIndex();
  Geom shape = new Rect(new Vector(1, 1), new Vector(1, 1));
  List<Geom> reg = new List();
  reg.add(shape);
  IndexEntry<String> index = new IndexEntry("box", reg);
  List<IndexEntry<String>> entries = [index];
  test.index(entries);
  print(reg.length);
  List<String> res = test.getIntersections(reg);
  print(res.length);
  res.forEach(print);
}