import re
import sys
ct = 1

sizing = "default"

if sizing== "default":
    gridsize = 7000
    scale= 1
    offset = 5000
elif sizing == "elering":
    gridsize = 12000
    scale= 10
    offset = 6000
elif sizing == "taskeast":
    scale = 2
    gridsize = 12000/scale
    offset = 6000/scale

def searchBox(c, lvl, ct): 
    typ = re.search("(Inner|Leaf)Box "+c, data).group(1)
    if (typ == "Inner"):
        for m in re.finditer("InnerBox "+c+" links to (.*?) \(", data):
            searchNode(m.group(1), lvl+1, ct)
    # for m in re.finditer("\w "+c+" contains ([0-9]+) \((-?[0-9]+), (-?[0-9])+\), \((-?[0-9]+), (-?[0-9]+)\)",data):

    # print(c)

def scaleVal(val):
    return (int(val) + scale - 1) / scale

def searchNode(c, lvl, ct):
    for m in re.finditer("(Inner|Leaf)Node "+c+" contains ([0-9]+) \((-?[0-9]+), (-?[0-9]+)\), \((-?[0-9]+), (-?[0-9]+)\)",data):
        (x, y) = int(m.group(3))/scale, int(m.group(4))/scale
        (width, height) = scaleVal(m.group(5)), scaleVal(m.group(6))
        if (m.group(1) == "Inner"):
            r = 255 - 25*lvl
            g = lvl*25
            b = 0
            sw = 2 #30 - lvl*4
            fill = "none"
        else:
            r = 0 #133 #ct*10
            g = 0 #lvl*25
            b = 255 #155 #- ct*20; ct+=1
            sw = 3
            fill = "none" #"rgb(0, 0, 0"
        print('<rect x="'+str(x+offset)+'" y="'+str(y+offset)+'" width="'+str((width-x))+'" height="'+str((height-y))+'" style="stroke:rgb('+str(r)+','+str(g)+','+str(b)+');stroke-width:'+str(sw)+';fill:'+fill+';fill-opacity=0;"/>')
        if (m.group(1) == "Inner"): searchBox(m.group(2), lvl, ct)


data = ''.join(sys.stdin.readlines())
start = re.search("\w (.)", data).group(1)
print("<html>\n<body>")
print("<svg width="+str(gridsize)+" height="+str(2*gridsize)+">")
searchNode(start, 0, 1)
print("</svg>")
print("</body>\n</html>")
