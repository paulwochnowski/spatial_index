Make naive version(s)
 - use MBRs for same results as RTree
 - don't use MBRs? (unnecessary)

Get MBRs of other geometries

Write unit tests
- insert
  - full node (leaf and inner)
  - multiple splits, going up the tree
- remove (Paul)
  - check root goes down when only 1 child remains
  - remove last object in tree
  - removing elements of a node does not affect other elements / other nodes
  - removing elements present in the overlap of different boxes
  - remove element that is not in the tree
- get intersections
  - intersects on a line / point
  - doesn't return everything in the node
  - returns things in different nodes

Write randomised tests

Implement split properly (Alex)
- quadratic
- exponential
- other?

fix removal?

questions
- how are duplicates handled

extensions
- drawing tree visually (in browser (w/ JS?) or otherwise)
- precision: augment to check against actual regions
  - find interior rectangle of objects whose MBR intersect query geometry and check if contained in MBR of query geometry? can result in cheap checks, but full check is necessary if it fails