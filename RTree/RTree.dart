library RTree;

import "../lib/lib.dart";
import "dart:collection";
import "dart:math";
import "dart:io";

part "Node.dart";
part "InnerNode.dart";
part "RTreeTest.dart";
part "LeafNode.dart";
part "Leaf.dart";

class RTree<T> {
  Node<T> _root;
  int _boxSize;
  int _minBoxSize;
  //Check whether we can still bulk insert
  bool constructed;
  HashMap<T, Leaf<T>> _store;

  RTree() {
    this._boxSize = 4;
    this._minBoxSize = (this._boxSize/2).floor();
    _root = new LeafNode(this._boxSize);
    _store = new HashMap();
    //don't do bulk insert at the start
    this.constructed = false;
  }

  //Basic sanity check function for testing used to ensure internals of
  //the tree make sense
  void sane() {
    _root._sane(null, _store);
  }

  //Output the tree in an obfuscating way into the given file. Use as input to
  //`parse.py` to translate into svg format which can be viewed in browser.
  void draw(File logFile) async {
    IOSink sink = logFile.openWrite();
    _root._draw(0, sink);
    await sink.flush();
    //await stdout.flush();
    await sink.close();
  }

  void insert(Leaf<T> entry) {
    Node<T> res = _root._insert(entry);
    _store[entry.val] = entry;

    if (res != null) {
      Node<T> newRoot = new InnerNode(this._boxSize);
      //Add current root as Box to newRoot
      newRoot._addToNode(_root);
      newRoot._addToNode(res);
      _root = newRoot;
    }
  }

  //Helper Function for bulkInsert
  List<Node<T>> group(List<Node<T>> l) {
    List<Node<T>> ret = new List();
    for (int i = 0; i < l.length; i += this._boxSize) {
      Node<T> n = new InnerNode(this._boxSize);
      for (int j = i; j < l.length && j - i < this._boxSize; j++) {
        print(i);
        n._addToNode(l[j]);
      }
      ret.add(n);
    }
    print("=\======\=====");
    return ret;
  }

  //Sort-Tile-Recursive (STR) algorithm
  //http://www.cs.du.edu/~leut/icdepaper.ps
  List<Node<T>> createLeaves(List<Leaf<T>> entries) {
    //sort by X
    entries.sort((e1, e2) => e1._minBoundingRectangle.centre.x.compareTo(e2._minBoundingRectangle.centre.x));
    int slices = sqrt((entries.length/this._boxSize).ceil()).ceil();
    int sliceSize = slices*this._boxSize;
    List<Node<T>> ret = new List();

    //Iterate over each slice between indices i and j
    for (int i = 0; i < slices; i++) {
      int start = i*sliceSize;
      int end = min(entries.length, start+sliceSize);
      if (end < start) {
        break;
      }
      int width = end - start;

      //Sort by Y
      List<Leaf<T>> currSlice = new List(width);
      currSlice.setRange(0, width, entries.getRange(start, end));
      currSlice.sort((e1, e2) => e1._minBoundingRectangle.centre.y.compareTo(e2._minBoundingRectangle.centre.y));

      for (int j=0; j < width; j+=this._boxSize) {
        Node<T> currNode = new LeafNode(this._boxSize);
        for (int k = j; k < width && k - j < this._boxSize; k++) {
          currNode._addToNode(currSlice[k]);

          _store[currSlice[k].val] = currSlice[k];
        }
        ret.add(currNode);
      }
    }
    return ret;
  }

  /*General bulk load strategy is as follows:
    * Notation: n - number of entries; b - entries per box
    * 1. Preprocess the data so that it is ordered in n/b groups of b rectangles
    * so that each group corresponds to a leaf level node. (last group might contain
    * fewer than b rectangles)
    * 2. Treat each group as a data entry and apply step 1 to pack these into
    * nodes at the next level, until the root node is created.
    * Different strategies typically only differ in how the rectangles are ordered
    * at each level.
    */
  void bulkInsert(List<Leaf<T>> entries) {
    this.constructed = true;

    List<Node<T>> res = createLeaves(entries);
    if (res.length == 1) {
      _root = res[0];
      return;
    }
    while (res.length > this._boxSize) {
      res = group(res);
      //res.sort(sortFun);
    }
    _root = new InnerNode(this._boxSize);
    for (Node<T> n in res) {
      _root._addToNode(n);
    }
  }

  //Fix up tree recursively after removing a leaf, to ensure all nodes
  //have < minBoxSize entries
  void _condenseTree(LeafNode<T> leaf) {
    Node<T> curr = leaf;
    List<Node<T>> removed = new List();
    Node<T> parent;
    while (curr != this._root) {
      parent = curr._parent;
      if (curr._size < this._minBoxSize) {
        //Remove the current level node (i.e. remove box corresponding to node)
        parent._remove(curr);
        removed.add(curr);
      } else {
        //Resize current node
        curr._updateMinBoundingRectangle();
      }
      curr = parent;
    }
    //Reinsert all entries from removed nodes
    List<Leaf<T>> vals = _extractValues(removed);
    for (Leaf<T> val in vals) {
      this.insert(val);
    }
  }

  List<Leaf<T>> _extractValues(List<Node<T>> vals) {
    List<Leaf<T>> res = new List();
    for (Node<T> val in vals) {
      res.addAll(val._getEntries());
    }
    return res;
  }

  void remove(T entry) {
    Leaf<T> tar = _store[entry];
    if (tar == null) {
      return;
    }
    this._store.remove(entry);
    tar.remove();
    if (this._root is LeafNode<T>) {
      return;
    }
    _condenseTree(tar._parent);
    if (_root._size == 1) {
      Node<T> node = _root._children[0];
      _root = node;
      _root._parent = null;
    }
  }

  int depth() {
    return _root._depth();
  }

  List<T> getIntersections(MinBoundingRectangle mbr) {
    return _root._getIntersections(mbr);
  }
}
