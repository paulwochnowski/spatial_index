part of RTree;

/* LeafNodes are at the bottom level of the tree.
 * They contain the actual objects stored in the tree.
 */
class LeafNode<T> extends Node<T> {

  LeafNode(int n): super(n);

  Node<T> _insert(Leaf<T> entry) {
    if (_size < _children.length) {
      this._addToNode(entry);
      this._updateMinBoundingRectangle();
      return null;
    }
    Node<T> newSplitNode = new LeafNode(_children.length);
    newSplitNode._addToNode(entry);

    return this._split(newSplitNode);
  }

  bool _correctChild(int n) {
    return this._children[n] is Leaf<T>;
  }

  List<Leaf<T>> _getEntries() {
    List<Leaf<T>> res = new List();
    for (int i = 0; i<this._size; i++) {
      res.addAll(this._children[i]._getEntries());
    }
    return res;
  }

  int _draw(int i, IOSink sink) {
    int p = i;
    for (int j = 0; j < _size; j++) {
      sink.writeln(
          "LeafNode ${p} contains ${i+1} ${this._children[j]._minBoundingRectangle}");
      this._children[j]._draw(i+1, sink);
      i++;
    }
    return i;
  }

  @override
  bool operator ==(dynamic other) {
    if (other is! LeafNode<T>) return false;
    LeafNode<T> n = other;
    return (n._children == this._children);
  }

}
