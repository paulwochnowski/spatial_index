part of RTree;

/* Node containing up to a specific number of children, constant for the tree.
 * There is a minimum number of children per Node, ceil(N/2).
 * There are two types of Nodes: Inner and Leaf. They contain Inner and Leaf
 *  boxes, respectively.
 * Upon insertion, if the Node is full, it is split into two new Nodes and
 *  elements are distributed between them.
 */
abstract class Node<T> extends NodeContents<T> {
  List<NodeContents<T>> _children;
  int _size;

  Node(int n) {
    _parent = null;
    _children = new List(n);
    _size = 0;
  }

  List<Leaf<T>> _getEntries() {
    List<Leaf<T>> res = new List();
    for (int i=0; i< this._size; i++) {
      res.addAll(this._children[i]._getEntries());
    }
    return res;
  }

  Node<T> _insert(Leaf<T> entry);

  void _addToNode(NodeContents<T> contents) {
    _children[_size++] = contents;
    contents._parent = this;
    this._updateMinBoundingRectangle();
  }

  void _remove(NodeContents<T> contents) {
    int i;
    for (i = 0; i < this._size; i++) {
      if (this._children[i] == contents) {
        this._size--;
        break;
      }
    }
    while (i < this._size) {
      this._children[i] = this._children[i + 1];
      i++;
    }
  }

  List<T> _getIntersections(MinBoundingRectangle minBoundingRectangle) {
    List<T> ret = new List();
    if (!this._minBoundingRectangle.intersects(minBoundingRectangle)) return ret;

    for (int i = 0; i < this._size; i++) {
      List<T> res = _children[i]._getIntersections(minBoundingRectangle);
      for (T tmp in res) {
        ret.add(tmp);
      }
    }
    return ret;
  }

  //Return best split
  Node<T> _split(Node<T> n) {
    List<NodeContents<T>> contents = new List();
    for (NodeContents<T> child in this._children) {
      contents.add(child);
    }
    contents.add(n._children[0]);
    this._size = 0;
    n._size = 0;

    return this._quadSplit(n, contents);
  }

  /* Quadratic split algorithm, finding the best box to insert into the two new
   *  nodes at each step.
   */
  Node<T> _quadSplit(Node<T> n, List<NodeContents<T>> contents) {
    // get the two MBRs that create the most dead space to seed the Nodes
    int minElems = (n._children.length/2).floor();
    int best = 0, bestI = 0, bestJ = 1;
    for (int i = 0; i < contents.length; i++) {
      for (int j = i + 1; j < contents.length; j++) {
        MinBoundingRectangle r1 = contents[i]._minBoundingRectangle, r2 = contents[j]._minBoundingRectangle;
        int deadArea = r1.join(r2).area - r1.unionArea(r2);

        if (deadArea > best) {
          best = deadArea;
          bestI = i;
          bestJ = j;
        }
      }
    }

    // seed the Nodes
    this._addToNode(contents[bestI]);
    n._addToNode(contents[bestJ]);
    MinBoundingRectangle r1 = contents[bestI]._minBoundingRectangle, r2 = contents[bestJ]._minBoundingRectangle;
    int size1 = 1, size2 = 1;

    contents.removeAt(bestJ);
    contents.removeAt(bestI);

    while (contents.length > 0) {
      // ensure nodes have the minimum number of children
      if (contents.length + size1 == minElems) {
        for (NodeContents<T> c in contents) {
          this._addToNode(c);
        }
        break;
      }
      if (contents.length + size2 == minElems) {
        for (NodeContents<T> c in contents) {
          n._addToNode(c);
        }
        break;
      }

      // get the object that has the greatest difference in dead space
      best = 0;
      bestI = 0;
      bool prefersR1 = true;
      for (int i = 0; i < contents.length; i++) {
        int dead1 = r1.join(contents[i]._minBoundingRectangle).area - r1.unionArea(contents[i]._minBoundingRectangle);
        int dead2 = r2.join(contents[i]._minBoundingRectangle).area - r2.unionArea(contents[i]._minBoundingRectangle);
        int diff = (dead1 - dead2).abs();
        if (diff > best) {
          best = diff;
          bestI = i;
          prefersR1 = (dead1 < dead2);
        }
      }

      // insert into the chosen box
      if (prefersR1) {
        this._addToNode(contents[bestI]);
        r1 = r1.join(contents[bestI]._minBoundingRectangle);
      } else {
        n._addToNode(contents[bestI]);
        r2 = r2.join(contents[bestI]._minBoundingRectangle);
      }
      contents.removeAt(bestI);
    }

    this._updateMinBoundingRectangle();
    n._updateMinBoundingRectangle();
    return n;
  }
  /* unimplemented
  Node<T> expSplit(Node<T> n, List<Box<T>> boxes) {
    return n;
  }*/

  void _updateMinBoundingRectangle() {
    MinBoundingRectangle res = this._children[0]._minBoundingRectangle;
    for (int i = 1; i < this._size; i++) {
      NodeContents<T> contents = this._children[i];
      res = res.join(contents._minBoundingRectangle);
    }
    this._minBoundingRectangle = res;
  }

  @override
  bool operator ==(dynamic other) {
    if (other is! InnerNode<T>) return false;
    InnerNode<T> iN = other;
    if (iN._size != this._size || iN._parent != this._parent) {
      return false;
    }

    for (int i = 0; i < this._size; i++) {
      if (this._children[i] != iN._children[i]) {
        return false;
      }
    }
    return true;
  }

  int _draw(int i, IOSink sink);

  int _depth() {
    int count = 0;
    for (int i = 0; i < this._size; i++) {
      count = max(count, this._children[i]._depth());
    }
    return count + 1;
  }

  void _sane(Node<T> n, HashMap<T, Leaf<T>> map) {
    assert(this._parent == n);
    assert(0 <= this._size && this._size <= 3);
    for (int i = 0; i < this._size; i++) {
      assert(this._children[i] != null);
      assert(this._correctChild(i));
      if (n != null) {
        assert(n._minBoundingRectangle.contains(this._children[i]._minBoundingRectangle));
      }
      this._children[i]._sane(this, map);
    }
  }

  //Returns true if i-th child of a Inner/Leaf node is a Inner/Leaf box respectively
  bool _correctChild(int i);

}

abstract class NodeContents<T>{
  Node<T> _parent;
  MinBoundingRectangle _minBoundingRectangle;

  List<T> _getIntersections(MinBoundingRectangle minBoundingRectangle);
  List<Leaf<T>> _getEntries();
  int _depth();
  void _sane(Node<T> n, HashMap<T, Leaf<T>> map);
  int _draw(int i, IOSink sink);
}