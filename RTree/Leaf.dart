part of RTree;

/* LeafBoxes contain the objects stored in the tree.
 */
class Leaf<T> extends NodeContents<T> {
  T val;

  Leaf._parent(Node<T> p, MinBoundingRectangle mbr, T entry) {
    this._parent = p;
    this._minBoundingRectangle = mbr;
    this.val = entry;
  }

  Leaf(MinBoundingRectangle mbr, T entry) {
    this._minBoundingRectangle = mbr;
    this.val = entry;
  }

  void remove() {
    this._parent._remove(this);
  }

  void _sane(Node<T> n, HashMap<T, Leaf<T>> map) {
    assert(this._parent == n);
    assert(this.val is T);
    assert(map[this.val] == this);
  }

  int _draw(int i, IOSink sink) {
    sink.writeln("LeafBox $i stores ${val.toString()}");
    return i;
  }

  List<Leaf<T>> _getEntries() {return [this];}
  

  @override
  bool operator ==(dynamic other) {
    if (other is! Leaf<T>) return false;
    Leaf<T> b = other;
    return (b.val == this.val && this._parent == other._parent);
  }

  int _depth() {
    return 0;
  }

  List<T> _getIntersections(MinBoundingRectangle mbr) {
    List<T> ret = new List();
    if (this._minBoundingRectangle.intersects(mbr)) ret.add(val);
    return ret;
  }

  @override
  String toString() {
    return "MBR: ${this._minBoundingRectangle}, val:${val.toString()}";
  }
}
