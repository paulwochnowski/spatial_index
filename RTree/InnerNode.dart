part of RTree;

/* InnerNodes contain Nodes lower in the tree.
 * When inserting, they find the child that would best contain the new entry
 *  and insert into it.
 */
class InnerNode<T> extends Node<T> {
  //@override
  //List<Node<T>> _children;

  InnerNode(int n): super(n);

  int _draw(int i, IOSink sink) {
    int curr = i;
    for (int j = 0; j < _size; j++) {
      sink.writeln(
          "InnerNode ${curr} contains ${i+1} ${this._children[j]._minBoundingRectangle}");
      i = this._children[j]._draw(i+1, sink);
    }
    return i;
  }

  bool _correctChild(int n) {
    return this._children[n] is Node<T>;
  }

  @override
  Node<T> _insert(Leaf<T> entry) {
      //Returns the amount of increase to the MBR if we insert the given rectangle
      int _extendCost(MinBoundingRectangle mbr) {
        MinBoundingRectangle large = this._minBoundingRectangle.join(mbr);
        return large.area - this._minBoundingRectangle.area;
      }

    //Find best fit box
    Node<T> bestNode = this._children[0];
    int min = _extendCost(entry._minBoundingRectangle);

    for (int i = 1; i < this._size; i++) {
      Node<T> n = this._children[i];
      int tmp = _extendCost(entry._minBoundingRectangle);
      // print("insert $mbr into ${b.mbr} with cost ${tmp}");
      if (tmp <= min) {
        if (tmp < min || n._minBoundingRectangle.area < bestNode._minBoundingRectangle.area) {
          min = tmp;
          bestNode = n;
        }
      }
    }

    //If tree had to split when inserting entry into bestBox return
    //the new node created during the split
    Node<T> ret = bestNode._insert(entry);

    if (ret == null) {
      //Managed to insert into bestBox without having to split anything
      this._updateMinBoundingRectangle();
      return null;
    }

    if (_size < _children.length) {
      this._addToNode(ret);
      ret = null;
    } else {
      Node<T> newSplitNode = new InnerNode(_children.length);
      newSplitNode._addToNode(ret);
      ret = this._split(newSplitNode);
      newSplitNode._parent = this._parent;
    }
    return ret;
  }

  @override
  bool operator ==(dynamic other) {
    if (other is! InnerNode<T>) return false;
    InnerNode<T> n = other;
    return (n._children == this._children);
  }
}
