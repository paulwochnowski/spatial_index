
part of RTree;

void testRemoveAll() {
  print("Setting up tree with 6 elements");
  RTree<MinBoundingRectangle> rt = new RTree();
  Vector o1 = new Vector(0, 0);
  Vector o2 = new Vector(500, 500);
  Vector size = new Vector(100, 100);
  List<MinBoundingRectangle> inserted = new List();
  Vector next = size;
  for (int i = 0; i < 3; i++) {
    inserted.add(new MinBoundingRectangle(o1, next));
    next = next + size;
  }
  next = size;
  for (int i = 0; i < 3; i++) {
    inserted.add(new MinBoundingRectangle(o2, next));
    next = next + size;
  }
  for (MinBoundingRectangle m in inserted) {
    rt.insert(new Leaf(m, m));
    rt.sane();
  }
  InnerNode<MinBoundingRectangle> dummy = rt._root;
  assert(dummy._size == 2);
  LeafNode<MinBoundingRectangle> lN = dummy._children[0];
  assert(lN._size == 3);
  lN = dummy._children[1];
  assert(lN._size == 3);
  print("Successfully set up Tree\nRemoving all items");
  lN = rt._root._children[0];
  for (int i = 0; i < 3; i++) {
    MinBoundingRectangle r = inserted[i];
    assert(rt._root is InnerNode<MinBoundingRectangle>);
    assert(rt._root._size == 2);
    rt.remove(r);
    assert(lN._size == 2-i);
    rt.sane();
    if (i == 0) continue;
  }
  assert(rt._root is LeafNode<MinBoundingRectangle>);
  for (int i = 0; i < 3; i++) {
    MinBoundingRectangle r = inserted[3 + i];
    rt.remove(r);
    assert(rt._root._size == 2 - i);
    rt.sane();
  }
  rt.draw(new File('log'));
}

void multiLevelInsert() {
  RTree<MinBoundingRectangle> rt = new RTree();
  int offset = 300;
  List<Leaf<MinBoundingRectangle>> inserted = new List();
  for (int i = 1; i <= 3; i++) {
    Vector o = new Vector(offset * i, offset);
    for (int j = 1; j <= 3; j++) {
      Vector d = new Vector(50 * j, 50 * j);
      MinBoundingRectangle mbr = new MinBoundingRectangle(o, d);
      inserted.add(new Leaf(mbr, mbr));
    }
  }
  Vector o = new Vector(405, 405);
  Vector d = new Vector(340, 40);
  MinBoundingRectangle mbr = new MinBoundingRectangle(o, d);
  inserted.add(new Leaf(mbr, mbr));
  rt.bulkInsert(inserted);
  rt.draw(new File('log'));
}

void basicInsert() {
  RTree<MinBoundingRectangle> rt = new RTree();
  int n = 10;
  Vector v = new Vector(n, n);
  Vector size = new Vector(-1, -1);
  Vector next = size;
  Node<MinBoundingRectangle> dummy;
  Leaf<MinBoundingRectangle> lB;
  LeafNode<MinBoundingRectangle> lN;
  for (int i = 1; i <= 3; i++) {
    MinBoundingRectangle r = new MinBoundingRectangle(v, next);
    rt.insert(new Leaf(r, r));
    dummy = rt._root;
    assert(dummy._size == i);
    assert(dummy is LeafNode);
    assert(dummy._children[i - 1] is Leaf);
    lB = dummy._children[i - 1];
    assert(lB.val == r);
    next = next + size;
  }
  Node<MinBoundingRectangle> save = dummy;
  MinBoundingRectangle m = new MinBoundingRectangle(v, next);
  rt.insert(new Leaf(m, m));
  dummy = rt._root;
  assert(dummy is InnerNode);
  lN = dummy._children[0];
  assert(lN == save);
  assert(lN._children[0] is Leaf);
  lB = lN._children[0];
}

void testCompression() {
  print("Test tree compression");
  RTree<MinBoundingRectangle> rt = new RTree();
  Vector o1 = new Vector(0, 0);
  Vector o2 = new Vector(300, 0);
  Vector o3 = new Vector(600, 0);
  Vector size = new Vector(50, 50);
  List<MinBoundingRectangle> inserted = new List();
  Vector next = size;
  inserted.add(new MinBoundingRectangle(new Vector(0, 150), new Vector(50, 50)));
  for (int i = 0; i < 3; i++) {
    inserted.add(new MinBoundingRectangle(o1, next));
    next = next + size;
  }
  next = size;
  for (int i = 0; i < 3; i++) {
    inserted.add(new MinBoundingRectangle(o2, next));
    next = next + size;
  }
  next = size;
  for (int i = 0; i < 3; i++) {
    inserted.add(new MinBoundingRectangle(o3, next));
    next = next + size;
  }
  for (MinBoundingRectangle m in inserted) {
    rt.insert(new Leaf(m, m));
    rt.sane();
  }
  assert(rt._root._size == 2);
  for (int i = 7; i < 10; i++) {
    rt.remove(inserted[i]);
    rt.sane();
  }
  assert(rt._root._size == 2);
  for (int i = 4; i < 7; i++) {
    rt.remove(inserted[i]);
    rt.sane();
  }
  assert(rt._root._size == 2);
}

//Generate a cross with 2 elements in center  and remove it
void testRemoveOverlap() {
  RTree<MinBoundingRectangle> rt = new RTree();
  Vector o1 = new Vector(400, 200);
  Vector o2 = new Vector(600, 100);
  Vector o3 = new Vector(400, 0);
  Vector o4 = new Vector(200, 100);
  Vector o5 = new Vector(405, 105);
  Vector o6 = new Vector(300, 110);
  Vector size = new Vector(50, 50);
  List<MinBoundingRectangle> inserted = new List();
  inserted.add(new MinBoundingRectangle(o1, size));
  inserted.add(new MinBoundingRectangle(o2, size));
  inserted.add(new MinBoundingRectangle(o3, size));
  inserted.add(new MinBoundingRectangle(o4, size));
  inserted.add(new MinBoundingRectangle(o5, new Vector(40, 40)));
  inserted.add(new MinBoundingRectangle(o6, new Vector(200, 25)));
  for (MinBoundingRectangle m in inserted) {
    rt.insert(new Leaf(m, m));
    rt.sane();
  }
  MinBoundingRectangle m = inserted[4];
  LeafNode<MinBoundingRectangle> lN = rt._store[m]._parent;
  List<Node<MinBoundingRectangle>> oldNode = new List();
  int oldSize = lN._size;
  for (int i = 0; i < rt._root._size; i++) {
    Node<MinBoundingRectangle> child = rt._root._children[i];
    if (lN != child) {
      oldNode.add(child);
    }
  }
  rt.remove(m);
  rt.sane();
  //Check that other nodes remain the same
  int offset = 0;
  for (int i = 0; i < rt._root._size; i++) {
    Node<MinBoundingRectangle> node = rt._root._children[i];
    if (node == lN) {
      assert(node._size == oldSize - 1);
      offset = 1;
    } else {
      // print(box.n.size);
      assert(oldNode[i - offset] == node);
      assert(node._size == oldNode[i - offset]._size);
    }
  }
}


main() {
  print("Insert tests\n");
  //Insert Tests
  basicInsert();
  multiLevelInsert();
  print("Passed\n\nRemove tests");
  //Remove Tests
  testRemoveAll();
  testCompression();
  testRemoveOverlap();
  print("all tests passed! you are awesome! :)");
}
