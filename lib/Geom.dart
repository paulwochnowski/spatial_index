part of lib;

abstract class Geom extends MBRType {
//  MBR getMBR();
  MinBoundingRectangle getMinBoundingRectangle();
}
