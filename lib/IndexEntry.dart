part of lib;

class IndexEntry<T> extends MBRType {
  T object;
  List<Geom> regions;

  IndexEntry(T this.object, List<Geom> this.regions);

  String toString() {
    MinBoundingRectangle dummy = this.getMinBoundingRectangle();
    return "${object}, ${dummy}";
  }

  MinBoundingRectangle getMinBoundingRectangle() {
    Iterable<MinBoundingRectangle> mbrs = regions.map((reg) => reg.getMinBoundingRectangle());
    return mbrs.reduce((mbr1, mbr2) => mbr1.join(mbr2));
  }
}
