part of lib;

abstract class SpatialIndex<T> {
  void index(List<IndexEntry<T>> entries);

  void remove(List<T> objects);

  //Debug
  void display();
  int depth();

  List<T> getIntersections(List<Geom> region);
}
