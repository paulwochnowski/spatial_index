part of lib;

class Rect implements Geom {
  Vector start;
  Vector size;

  int get area => size.x * size.y;
  Vector get centre => new Vector(start.x + (size.x/2).round(), start.y + (size.y/2).round());
  int get minX => min(start.x, start.x + size.x);
  int get maxX => max(start.x, start.x + size.x);
  int get minY => min(start.y, start.y + size.y);
  int get maxY => max(start.y, start.y + size.y);


  Rect.empty() {
    this.start = new Vector.zero();
    this.size = new Vector.zero();
  }

  int compareTo(Rect other) {
    if (this.centre.x != other.centre.x) {
      return this.centre.x.compareTo(other.centre.x);
    }
    return this.centre.y.compareTo(other.centre.y);
  }

  Rect(this.start, this.size);

  MinBoundingRectangle getMinBoundingRectangle() =>
      new MinBoundingRectangle(new Vector.from(this.start),
                                new Vector.from(this.size));

  String toString() {
    return this.getMinBoundingRectangle().toString();
  }

  Rect normal() {
    Rect r = new Rect(new Vector.from(this.start), new Vector.from(this.size));
    if (r.size.x < 0) {
      r.start.x += r.size.x;
      r.size.x *= -1;
    }
    if (r.size.y < 0) {
      r.start.y += r.size.y;
      r.size.y *= -1;
    }
    return r;
  }

  @override
  bool operator ==(dynamic other) {
    if (other is! Rect) return false;
    Rect r = other;
    Rect r1 = this.normal();
    Rect r2 = r.normal();
    return (r1.start == r2.start && r1.size == r2.size);
  }
}
