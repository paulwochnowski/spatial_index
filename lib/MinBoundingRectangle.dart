part of lib;

class MinBoundingRectangle extends Rect {
  MinBoundingRectangle(Vector start, Vector size) : super(start, size) {
    Rect n = this.normal();
    this.start = n.start;
    this.size = n.size;
  }

  MinBoundingRectangle.empty() : super.empty();

  MinBoundingRectangle getMinBoundingRectangle() => this;

  MinBoundingRectangle join(MinBoundingRectangle mbr) {
    /*if (this.start.x == 0 && this.start.y == 0 && this.size.x == 0 && this.size.y == 0) return mbr;
    if (mbr.start.x == 0 && mbr.start.y == 0 && mbr.size.x == 0 && mbr.size.y == 0) return this;*/

    Vector botLeft = new Vector(
        min(this.minX, mbr.minX), min(this.minY, mbr.minY));
    Vector topRight = new Vector(
        max(this.maxX, mbr.maxX), max(this.maxY, mbr.maxY));
    return MinBoundingRectangle(botLeft, topRight - botLeft);
  }

  int unionArea(MinBoundingRectangle mbr) {
    int intersection = this.intersectionArea(mbr);
    if (intersection < 0) intersection = 0;
    return this.area + mbr.area - intersection;
  }

  @override
  String toString() {
    Vector end = this.start + this.size;
    return "$start, $end";
  }

  bool contains(MinBoundingRectangle mbr) {
    if (this.start.x > mbr.start.x && this.start.y > mbr.start.y) {
      return false;
    }
    return (mbr.start.x + mbr.size.x <= this.start.x + this.size.x &&
        mbr.start.y + mbr.size.y <= this.start.y + this.size.y);
  }

  int intersectionArea(MinBoundingRectangle mbr) {
    //Find corners of the rectangle given by the intersection
    Vector bottomLeft = new Vector(max(this.minX, mbr.minX), max(this.minY, mbr.minY));
    Vector topRight = new Vector(min(this.maxX, mbr.maxX), min(this.maxY, mbr.maxY));
   
    if (bottomLeft.x > topRight.x || bottomLeft.y > topRight.y) return -1;

    return (topRight.x - bottomLeft.x) * (topRight.y - bottomLeft.y);
  }

  bool intersects(MinBoundingRectangle mbr) {
    return (this.intersectionArea(mbr) != -1);
  }
}
