part of lib;

class PolyLine extends Geom {
  List<Vector> points;

  PolyLine(this.points);
  PolyLine.fromJSON(List l) {
    this.points = new List();
    for (var p in l) {
      this.points.add(new Vector((p["x"]).round(), (p["y"]).round()));
    }
  }

  String toString() {
    String s = "";
    for (Vector p in points) {
      s += ("(${p.x}, ${p.y}) ");
    }
    return s;
  }

  MinBoundingRectangle getMinBoundingRectangle() {
    MinBoundingRectangle res = new MinBoundingRectangle(this.points[0], this.points[1] - this.points[0]);
    for (int i=2; i<this.points.length; i++) {
      MinBoundingRectangle mbr = new MinBoundingRectangle(this.points[i-1], this.points[i] -this.points[i-1]);
      res = res.join(mbr);
    }
    return res;
  }
}