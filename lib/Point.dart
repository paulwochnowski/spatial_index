part of lib;

class Point extends Geom {
  Vector p;
  int get x => p.x;
  int get y => p.y;

  Point(int x, int y): p = new Vector(x,y);

  String toString() {
    return "${p.x} ${p.y}";
  }
  MinBoundingRectangle getMinBoundingRectangle() {
    return new MinBoundingRectangle(new Vector.from(this.p), new Vector(1,1));
  }
}