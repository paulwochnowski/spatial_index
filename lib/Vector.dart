part of lib;

enum Side {
  left, right, neither
}

class Vector {
  int x, y;

  Vector.zero() {
    this.x = 0;
    this.y = 0;
  }

  Vector(this.x, this.y);

  Vector.from(Vector v) {
    this.x = v.x;
    this.y = v.y;
  }

  Vector operator +(Vector v) => new Vector(x + v.x, y + v.y);
  Vector operator -(Vector v) => new Vector(x - v.x, y - v.y);
  Vector operator /(int n) => Vector((this.x / n).floor(), (this.y / n).floor());


  // Determine on if current point is on the left or right side of the line going from p1 through p2
  Side orientation(Vector p1, Vector p2) {
    int crossProductSign = ((this.y-p1.y)*(p2.x-p1.x) - (this.x-p1.x)*(p2.y-p1.y)).sign;
    //TODO Put epsilon check here when supporting floating point coordinates
    if (crossProductSign == 0) {
      return Side.neither;
    }
    return (crossProductSign > 0) ? Side.right : Side.left;
  }

  String toString() {
    return "($x, $y)";
  }

  @override
  bool operator ==(dynamic other) {
    if (other is! Vector) return false;
    Vector v = other;
    return (v.x == x && v.y == y);
  }
}
