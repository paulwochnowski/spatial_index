for i in `seq 5`
do
    seed=$RANDOM
    count=5000
    dart bin/Tester.dart nR $count $seed q n > exp.txt
    dart bin/Tester.dart r $count $seed q n > out.txt

    #remove adjacent duplicates
    #uniq "tmp.txt" >out.txt
    #rm tmp.txt

    diff "exp.txt" "out.txt"
    # pattern="<"
    # if test "$(diff "exp.txt" "out.txt" | grep "$pattern" | head -c1)" != "" ; then
    #     echo "naive returns rectangles not returned by implementation"
    # fi
done