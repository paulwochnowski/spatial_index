
import '../lib/lib.dart';
import 'dart:collection';
import "dart:math";
import 'dart:io';


class Pair{
  int x;
  int y;
  Pair(this.x, this.y);

  @override
  bool operator ==(dynamic other) {
    // TODO: implement ==
    if (other is! Pair) {
      return false;
    }
    Pair p = other;
    return (this.x == p.x && this.y == p.y);
  }

  @override
  int get hashCode {
    return "$x$y".hashCode;
  }
}


typedef void Action(int a, int b);

//SparseGrid allows you to store & query geometries that implement the Geom class
//Objects are stored in the tiles, of a grid, that are assosciated to the space
//they occupy. Each tile corresponds to the co-ordinates of the bottom left corner
//point and extends `_offsetY` units in the +y directon and `_offsetX` units in +x.
//The y axis in the co-ordinate system used by the grid is inverted and
// the boxes are referred to by tuple of indices which is obtained by
// scaling the coordinates of the vertex with the smallest (x,y) coordinates.
// so each tile should be viewed the following way:
/*
oX, oY <-> offsetX/offsetY
The tile representing the following tile has
indexX = floor(x/oX) and indexY = floor(y/oY)
                     -> +x
    (x, y)___________Bottom_________(x + oX, y)
    |                                    |
   Left                                Right  | +y
    |                                    |    v
    (x, y + oY) -----Top------(x + oX, y + oY)
 */
class SparseGrid<T> {
  Map<Pair, List<T>> _grid;
  Map<T, List<Pair>> _store;
  int _offsetX, _offsetY;
  //Only used for displaying grid nicely
  int startX, endX;
  int startY, endY;


  int _xAxisCoordinate(int i) => _offsetX * i;
  int _yAxisCoordinate(int j) => _offsetY * j;


  //Return x-coordinate of nearest vertical grid axis
  int _getXIndex(int x) => (x / this._offsetX).floor();
  //Return y-coordinate of nearest horizontal grid axis
  int _getYIndex(int y) => (y / this._offsetY).floor();


  SparseGrid(int offsetX, int offsetY) {
    this._grid = new HashMap();
    this._store = new HashMap();
    this._offsetX = offsetX;
    this._offsetY = offsetY;
    this.startX = this.startY = this.endX = this.endY = null;
  }

  void _process(Geom g, Action action) {
    if (g is PolyLine) {
      _polylineTiles(g, action);
    }
    else if (g is Point) {
      _pointTiles(g, action);
    } else {
      _rectangleTiles(g.getMinBoundingRectangle(), action);
    }
  }

  Set<T> query(List<Geom> region) {
    Set<T> ret = new Set();
    //Closure to insert elements into ret
    void get(int i, int j) {
      Pair p = new Pair(i, j);
      if (this._grid.containsKey(p)) {
        ret.addAll(this._grid[p]);
      }
    }

    for (Geom g in region) {
      _process(g, get);
    }
    return ret;
  }

  //Visit all tiles associated for geometry
  void _polylineTiles(PolyLine p, Action action) {
    List<Vector> points = p.points;
    int indexX, indexY;
    for (int i = 1; i < points.length; i++) {
      Vector currPoint = new Vector(points[i - 1].x, points[i - 1].y);
      indexX = _getXIndex(currPoint.x);
      indexY = _getYIndex(currPoint.y);
      Vector endPoint = new Vector(points[i].x, points[i].y);
      int xDiff = endPoint.x - currPoint.x;
      int yDiff = endPoint.y - currPoint.y;

      if (currPoint == endPoint) {
        continue;
      }
      //loop until endpoint is not in the grid whose top left corner is at
      //the coordinates represented by indexX, indexY
      while ( indexX != _getXIndex(endPoint.x) || indexY != _getYIndex(endPoint.y)) {

        //Perform the action (insert or retrieve)
        action(indexX, indexY);

        Vector cornerPoint = new Vector(
            _xAxisCoordinate((xDiff <= 0) ? indexX : indexX + 1),
            _yAxisCoordinate((yDiff <= 0) ? indexY : indexY + 1));

        //Determine if the point `endPoint` lies left or right
        //of the line from currPoint to cornerPoint
        Side side = endPoint.orientation(currPoint, cornerPoint);

        // y-axis is flipped so each grid should be thought of with the top
        // and bottom edge flipped.
        if (xDiff == 0 || yDiff == 0) {
          if (xDiff == 0) indexY += (yDiff > 0) ? 1 : -1;
          if (yDiff == 0) indexX += (xDiff > 0) ? 1 : -1;
        }
        else if (xDiff < 0) {
          if (yDiff > 0) {
            //Corner point is Top Left
            if (side == Side.left) {
              //Line goes through top edge
              indexY++;
            } else if (side == Side.right) {
              //Line goes through left edge
              indexX--;
            } else {
              indexX--;
              indexY++;
            }
          } else {
            //Corner point is Down Left
            if (side == Side.left) {
              //Line goes through left edge
              indexX--;
            } else if (side == Side.right) {
              //Line goes through bottom edge
              indexY--;
            } else {
              indexX--;
              indexY--;
            }
          }
        } else {
          if (yDiff > 0) {
            //Corner point is Up Right
            if (side == Side.left) {
              //Line goes through right edge
              indexX++;
            } else if (side == Side.right) {
              //Line goes through top edge
              indexY++;
            } else {
              indexY++;
              indexX++;
            }
          } else {
            //Corner point is Down Right
            if (side == Side.left) {
              //Line goes through bottom edge
              indexY--;
            } else if (side == Side.right) {
              //Line goes through right edge
              indexX++;
            } else {
              indexY--;
              indexX++;
            }
          }
        }
      }
    }
    //Account for last tile of line
    action(indexX, indexY);
  }

  void _rectangleTiles(Rect rect, Action action) {
    //Normalise rectangle so that (minX, minY) correspond to bottom left point
    Rect r = rect.normal();
    int indexX = _getXIndex(r.minX);
    int indexY = _getYIndex(r.minY);

    for (int j = indexY; j * _offsetY < r.maxY; j++) {
      for (int i = indexX; i * _offsetX < r.maxX; i++) {
        action(i, j);
      }
    }
  }

  void _pointTiles(Point p, Action action) {
    int i = _getXIndex(p.x);
    int j = _getYIndex(p.y);
    action(i, j);

    if (p.x == this._offsetX * i) {
      action(i-1,j);
    }
    if (p.y == this._offsetY * j) {
      action(i,j-1);
    }
  }

  void display() async {
    File logFile = new File('SparseGridLog.html');
    IOSink sink = logFile.openWrite();
    sink.writeln("<html>\n<body>\n<table>");
    String white = '"FFFFFF"';
    String black = '"000000"';

    for (int y = this.startY; y <= this.endY; y+=this._offsetY) {
      sink.writeln("<tr>");
      for (int x = this.startX; x <= this.endX; x+=this._offsetX) {
        Pair p = new Pair(_getXIndex(x),_getYIndex(y));
        sink.writeln("\t<th bgcolor=${_grid.containsKey(p) ? "$white > ${_grid[p].length}" : "$black > 0"} <\/th>");
      }
      sink.writeln("<\/tr>");
    }
    sink.writeln("<\/table>\n<\/body><\/html>");
    await sink.flush();
    await sink.close();
  }

  void insert(IndexEntry<T> entry) {
    MinBoundingRectangle tmp = entry.regions[0].getMinBoundingRectangle();
    if (tmp.minX.abs() > 10000 || tmp.maxX > 10000 || tmp.maxY > 10000 || tmp.minY.abs() > 10000) return;
    if (this.startX == null) {
      this.startX = tmp.minX;
      this.endX = tmp.maxX;
      this.startY = tmp.minY;
      this.endY = tmp.maxY;
    }
    this.startX = min(this.startX, tmp.minX);
    this.endX = max(this.endX, tmp.maxX);
    this.startY = min(this.startY, tmp.minY);
    this.endY = max(this.endY, tmp.maxY);

    this._store[entry.object] = new List();
    void add(int x, int y) {
      Pair p = new Pair(x,y);
      if (!this._grid.containsKey(p)) {
        this._grid[p] = new List();
      }
      this._store[entry.object].add(p);
      this._grid[p].add(entry.object);
    }
    for (Geom g in entry.regions) {
      _process(g, add);
    }
  }

  void remove(T object) {
    if (this._store.containsKey(object)) {
      for (Pair p in this._store[object]) {
        this._grid[p].remove(object);
        if (this._grid[p].length == 0) {
          this._grid.remove(p);
        }
      }
      this._store.remove(object);
    }
  }

}