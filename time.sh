get_time()
{
    total=0
    for i in `seq 5`;
    do
        time="$( (/usr/bin/time -f %U dart bin/Tester.dart $1 $2 $3 $4 $5 > /dev/null) 2>&1)"
        #echo $string
        #time="$( echo $(( $( echo $string | sed -E "s/ .*//" ) * 60 + $( echo $string | sed -E "s/.* //; s/\..*//" ) ))$( echo $string | sed -E "s/^.*\.//" ) | sed -E "s/^0*([0-9])/\1/" )"
        total="$(echo $total $time | awk '{print $1 + $2}')"

        #echo $time
        #total=$(($total + $time))
    done
    total="$(echo $total | awk '{print $1 / 5}')"
    #echo "average:" $total
    echo $total
}

n_insert="$(get_time n $1 0 i $2)"
echo "naive insert: " $n_insert
n_query=$(get_time n $1 0 q $2)
echo "naive time: " $(echo $n_query $n_insert | awk '{print $1 - $2}')
r_insert=$(get_time r $1 0 i $2)
echo "rtree insert:" $r_insert
r_query=$(get_time r $1 0 q $2)
echo "rtree time: " $(echo $r_query $r_insert | awk '{print $1 - $2}')